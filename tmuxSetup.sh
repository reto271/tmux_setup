#!/bin/bash
#------------------------------------------------------
# RTController tmux setup script
#------------------------------------------------------

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null

DirPath=$1
eCompiler=$2
if [ "$DirPath" == "" ] ; then
    echo "Please specify dir"
    exit -1
else
    DIR_NAME=${DirPath}
    DIR_PATH=${SCRIPTDIR}/${DIR_NAME}
    DIR_PATH_CONTAINER=${HOME}/${DIR_NAME}
fi
if [ "$Compiler" == "" ] ; then
    Compiler=clang
fi
#SessionName=`realpath --relative-to=Work $DirPath`
#SessionName=${SessionName/ /_}
#SessionName=${SessionName/./_}
SessionName=${DIR_NAME}
TTY=`tty`
ttyNum=`basename $TTY`
ttyNum=$(($ttyNum+2))

tmux new-session -d -s $SessionName
#tmux set-environment -t ${SessionName} ECAT_LOG_TERMINAL /dev/pts/$ttyNum
tmux send-keys -t ${SessionName} "kmxc ssh rtcdev" Enter
tmux send-keys -t ${SessionName} "cd ${DIR_PATH_CONTAINER}/build/clang-inst/bin" Enter
tmux send-keys -t ${SessionName} "mountnetworkdrives" Enter
tmux split-window -t ${SessionName} -v -t 1 -c ${DIR_PATH}/build/$Compiler-inst/bin
tmux send-keys -t ${SessionName} "kmxc ssh rtcdev" Enter
tmux send-keys -t ${SessionName} "cd ${DIR_PATH_CONTAINER}" Enter
tmux split-window -t ${SessionName} -h -t 1 -c ${DIR_PATH}


tmux new-window -t ${SessionName} -n feature -c $DIR_PATH
tmux send-keys -t ${SessionName} "cd scripts" Enter
tmux split-window -t ${SessionName} -v -t 1 -c ${DIR_PATH}/build/$Compiler-inst/bin
tmux split-window -t ${SessionName} -h -t 1 -c pycharm-community-2020.2.3/bin


tmux attach-session -t ${SessionName}


# Back to the original location
popd > /dev/null
